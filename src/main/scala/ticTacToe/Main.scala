package ticTacToe

import akka.actor.{ActorSystem, Props}
import ticTacToe.actors.{StartMessage, TicTacToeActor}
import ticTacToe.views.CoordinateControlerView

object Main extends App {
  val coordinateControler = CoordinateControlerView.read
  val system = ActorSystem("system")
  val actorX = system.actorOf(Props(classOf[TicTacToeActor], coordinateControler, 'X'), "actorX")
  val actorO = system.actorOf(Props(classOf[TicTacToeActor], coordinateControler, 'O'), "actorO")

  actorX ! StartMessage(actorO)
}
