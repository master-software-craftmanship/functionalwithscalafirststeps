package ticTacToe.actors

import akka.actor.{Actor, ActorRef}
import ticTacToe.models.Game
import ticTacToe.views.{CoordinateView, GameView, GestorIO}

class TicTacToeActor(coordinateControler: CoordinateView, player: Char) extends Actor {

  private val coordinateControler_ : CoordinateView = coordinateControler

  private def play(mate: ActorRef, game: Game) = {
    GameView.write(game)
    if (game.isTicTacToe) {
      GestorIO.write("... pero has perdido")
      mate ! StopMessage
      GestorIO.write("Parando actor " + player)
      context.stop(self)
      context.system.terminate()
    } else {
      if (!game.isComplete){
        mate ! GameMessage(game.put(coordinateControler_.read))
      } else {
        mate ! GameMessage(game.move(coordinateControler_.read, coordinateControler_.read))
      }
    }
  }

  def receive = {
    case StartMessage(mate) => {
      play(mate, new Game())
    }
    case StopMessage =>
      GestorIO.write("Parando actor " + player)
      context.stop(self)
    case GameMessage(game) => {
      play(sender, game)
    }
  }
}