package ticTacToe.actors

import akka.actor.ActorRef
import ticTacToe.models.Game

case class GameMessage(value: Game)
case class StartMessage(respondTo: ActorRef)
case object StopMessage