package ticTacToe.views.Automatic

import ticTacToe.models.Coordinate
import ticTacToe.views.CoordinateView

import scala.util.Random

object AutomaticCoordinateView extends CoordinateView {
  private var randomGenerator = new Random()

  def read:Coordinate = {
    val row = randomGenerator.nextInt(3)
    val column = randomGenerator.nextInt(3)
    new Coordinate(row, column)
  }

}
