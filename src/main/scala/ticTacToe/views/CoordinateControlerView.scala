package ticTacToe.views

import ticTacToe.views.Automatic.AutomaticCoordinateView
import ticTacToe.views.Manual.ManualCoordinateView

object CoordinateControlerView {

  def read:CoordinateView = {
    val playerKind = GestorIO.readChar("[A]utomatico o [M]anual? [A,M]")
    playerKind match  {
      case 'A' => AutomaticCoordinateView
      case 'M' => ManualCoordinateView
    }

  }

}
