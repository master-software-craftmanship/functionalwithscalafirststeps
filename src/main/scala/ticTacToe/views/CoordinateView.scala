package ticTacToe.views

import ticTacToe.models.Coordinate

trait CoordinateView {

  def read:Coordinate

}
