import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class Quicsort {
  def sort(list: List[Int]): List[Int] = {
    list match {
      case Nil => Nil
      case head::Nil => List(head)
      case pivote::tail => {
        val menores = Future {
          sort(tail.filter(_ < pivote))
        }
        val mayores = Future {
          sort(tail.filter(_ >= pivote))
        }
        Await.result(menores, 100 seconds) ::: List(pivote) ::: Await.result(mayores, 100 seconds)
      }
    }
  }
}

val list = 1 :: 2 :: 3 :: 1 :: 0 :: 3 :: Nil
val qs = new Quicsort()

qs.sort(list)

